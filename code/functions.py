import numpy as np
import scipy as sp
import scipy.stats
import numpy.random as random
import matplotlib.pyplot as plt
import pandas as pd
import random
import scipy.linalg as alg



#this function works also for random data, can be modified in case of "full" ones
def countmemb(itr):
    d = np.zeros(np.max(itr))

    for val in itr:
        d[val-1] += 1

    return d


def sum_on_all(a, N_sl, k, N_s, iss):
    #limitiamo il numero di liste 
    K = len(a)
    
    res = np.zeros(iss[k])
    kr = np.array(range(K))
    for l in kr[kr!= k]:
        res += np.matmul(N_sl[k][l],a[l])
    
    for j in range(iss[k]):
        if N_s[k][j] != 0:
            res[j] = res[j]/ N_s[k][j]
        else:
            res[j] = 0

    return res


def distance(val_1, val_2,data):
    aux = sum([sum(np.power(val_1.a[k]-val_1.a[k],2)) for k in range(len(val_1.a))])/sum(data.iss) + (val_1.mu-val_2.mu)**2 + (val_1.tau_e-val_2.tau_e)**2+sum(np.power(val_1.tau-val_2.tau,2))/data.K
    
    if aux!= 0:
        dist = -np.log(aux)
    else:
        dist = 50
 
    return dist



class Data_sim:
    def __init__(self,N,K,I,mu):
        self.K = K
        self.I = I
        self.N = N
        self.mu = mu
        
        xk =  1+np.arange(I) #di fatto è l'assignement di ogni elemento 
        pk = [1/I for i in range(I)]
        
        custm = sp.stats.rv_discrete(name='custm', values=(xk, pk))
        self.ii = custm.rvs(size=(N,K)) 
        self.iss = [np.max(self.ii[:,k]) for k in range(self.K)] 
        a = np.random.normal(0,1, size = (self.I,self.K))
        aux = np.sum(np.array([[a[self.ii[n,j]-1,j] for j in range(self.K)] for n in range(self.N)]), axis = 1)
        
        self.y = np.array([aux[i]+mu+np.random.normal(0,1) for i in range(N)])
        self.iss = [np.max(self.ii[:,k]) for k in range(K)] 
        self.mean_y = np.mean(self.y)
        self.mean_y_s = np.array([[np.mean(self.y[self.ii[:,k] == h]) if not np.isnan(np.mean(self.y[self.ii[:,k] == h])) else 0 for h in range(1,self.iss[k]+1)] for k in range(self.K) ])
        self.N_s = np.array([countmemb(self.ii[:,k]) for k in range(self.K)])
        self.N_sl = [[ np.zeros((self.iss[i],self.iss[j])) for j in range(self.K)] for i in range(self.K)]
        for i in range(self.K) :
            for j in range(self.K):
                for n in range(self.N):
                    self.N_sl[i][j][self.ii[n,i]-1, self.ii[n,j]-1] += 1


def maximal_coupling_normal(mu_x, mu_y, var_x, var_y):
    x = np.random.normal(mu_x, np.sqrt(var_x))
    w = np.random.rand()*normal_dens(mu_x,np.sqrt(var_x),x)
    
    if w < normal_dens(mu_y,np.sqrt(var_y),x):
        return (x,x)
    else:
        while True:
            y = np.random.normal(mu_y, np.sqrt(var_y))
            w = np.random.rand()*normal_dens(mu_y,np.sqrt(var_y),y)
            if w > normal_dens(mu_x,np.sqrt(var_x),y):
                return (x,y)
            
def maximal_coupling_gamma(a_x,a_y, b_x, b_y):
    x = np.random.gamma(a_x, b_x)
    w = np.random.rand()
    
    if w < np.power(b_x/b_y,a_y)*np.exp(x*(1/b_x-1/b_y)):
        return (x,x)
    else:
        while True:
            y = np.random.gamma(a_y, b_y)
            w = np.random.rand()
            if w > np.power(b_y/b_x,a_x)*np.exp(y*(1/b_y-1/b_x)):
                return (x,y)

def normal_dens(mu,sigma,x): #mind i pass only sigma
    return 1/np.power(2*np.pi*sigma**2,0.5)*np.exp(-0.5*np.power(x-mu,2)/np.power(sigma,2))
    

def reflection_coupling(mu_1, mu_2, sigma):
    u = np.random.rand()
    sigma_1= alg.inv(sigma)
    sigma_sq = alg.sqrtm(sigma)
    X = np.random.normal(0,1, size = len(mu_1))
    z = np.matmul(alg.inv(sigma_sq), mu_1-mu_2)
    e = z/alg.norm(z)
    if u< np.exp(-0.5*(np.transpose(z)@np.identity(len(z))@z+2*np.transpose(z)@np.identity(len(z))@X)):
        Y=X+z
    else:
        Y=X-2*np.matmul(np.transpose(e),X)*e
    X = mu_1+ np.matmul(sigma_sq,X)
    Y = mu_2+ np.matmul(sigma_sq,Y)
    return X,Y




class Data_df:
    def __init__(self,df):
        #try with real dataset 
        self.y = df['y']
        self.ii = np.array(df[['s','dept']])
        self.I = self.ii.max()
        self.K = self.ii.shape[1]
        self.N= self.ii.shape[0]
        self.iss = [np.max(self.ii[:,k]) for k in range(self.K)] 
        self.mean_y = np.mean(self.y)
        self.mean_y_s = np.array([[np.mean(self.y[self.ii[:,k] == h]) if not np.isnan(np.mean(self.y[self.ii[:,k] == h])) else 0 for h in range(1,self.iss[k]+1)] for k in range(self.K) ])
        self.N_s = np.array([countmemb(self.ii[:,k]) for k in range(self.K)])
        self.N_sl = [[ np.zeros((self.iss[i],self.iss[j])) for j in range(self.K)] for i in range(self.K)]
        for i in range(self.K) :
            for j in range(self.K):
                for n in range(self.N):
                    self.N_sl[i][j][self.ii[n,i]-1, self.ii[n,j]-1] += 1
            
class iter_value_r:    
    def __init__(self, I,K,iss):
        self.tau = np.random.gamma(1/4,4, size= K)
        self.tau_e = np.random.gamma(1/4,4)
        self.mu = np.random.normal(0,1/np.sqrt(self.tau_e))
        self.a = [np.random.normal(0,1/np.sqrt(self.tau[k]),size=iss[k]) for k in range(K)]

class iter_value:    
    def __init__(self, I,K,iss):
        self.tau = np.ones(K)*0.5
        self.tau_e = 0.5
        self.mu = np.random.normal(0,1)
        self.a = [np.random.normal(0,1/self.tau[k],size=iss[k]) for k in range(K)]
        
        


def MCMC_sampler_final_2(data,T, collapsed, PX,L=1, verbose= False):
    
    N = data.N
    K = data.K
    val_1 = iter_value_r(data.I,data.K, data.iss)
    val_2 = iter_value_r(data.I,data.K, data.iss)

    a_means_1 = [sum(val_1.a[k] * data.N_s[k])/N for k in range(data.K)]
    a_means_2 = [sum(val_2.a[k] * data.N_s[k])/N for k in range(data.K)]
    mu_chain_1 = []
    mu_chain_2 = []
    tau_chain_1= np.zeros((K,T))
    tau_chain_2= np.zeros((K,T))
    tau_e_1 = []
    tau_e_2 = []
    a_means_chain_1 = np.zeros(shape=(data.K,T))
    a_means_chain_2 = np.zeros(shape=(data.K,T))
    SS0_chain_1 = []
    SS0_chain_2 = []
    dist = [distance(val_1, val_2, data)]
    t_fin = 0
    coupling = False
    
    
    #add the lag in the coupling, only in one chain
    
    for t in range(L): #need to sample only from one chain
        pred_1 = 0
        for k in range(data.K):
            s_k_1 = data.N_s[k]*val_1.tau_e / (val_1.tau[k]+ data.N_s[k]*val_1.tau_e)
            sum_a_l_1 =  sum_on_all(val_1.a,data.N_sl,k, data.N_s,data.iss)
            
            if collapsed:
                mu_mean_1= sum( s_k_1 *(data.mean_y_s[k] - sum_a_l_1))/sum(s_k_1)
                var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                aux = np.random.normal(0,1)
                val_1.mu = mu_mean_1+aux*np.sqrt(var_mean_1)
                    
               
            a_k_mean_1 = val_1.tau_e*data.N_s[k]/(val_1.tau_e*data.N_s[k]+val_1.tau[k])*(np.array(data.mean_y_s[k])-val_1.mu-sum_a_l_1)
            a_k_var_1 = 1/(data.N_s[k]*val_1.tau_e+val_1.tau[k])  
            aux = np.random.normal(0,1, size= data.iss[k])
            val_1.a[k]= a_k_mean_1+ np.sqrt(a_k_var_1)*aux
            a_means_1[k] = sum(val_1.a[k]*data.N_s[k])/N

            if PX :
                prec_alpha_k_1= val_1.tau_e*sum(data.N_s[k]*np.power(val_1.a[k],2))
                mean_alpha_k_1 = sum(val_1.a[k]*(data.N_s[k]*val_1.tau_e+val_1.tau[k])*a_k_mean_1)/prec_alpha_k_1
                aux = np.random.normal(0,1, size= 1)
                alpha_k_1 = mean_alpha_k_1+ aux/np.sqrt(prec_alpha_k_1)
                val_1.a[k] = val_1.a[k]*alpha_k_1

            pred_1 += val_1.a[k][data.ii[:,k]-1]
            val_1.tau[k] = np.random.gamma(size=1, shape= data.iss[k]/2-0.5, scale= 2/sum(np.power(val_1.a[k],2)))
            
        pred_1 += val_1.mu
        SS0_1 = sum(np.power((data.y-pred_1),2))
        val_1.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_1)
    
    # now start the algorithm for sampling 
    for t in range(T):
        print(t, end='\r')

        mu_chain_1.append(val_1.mu)
        mu_chain_2.append(val_2.mu)
        a_means_chain_1[:,t]=a_means_1
        a_means_chain_2[:,t]=a_means_2
        tau_chain_1[:,t] = val_1.tau
        tau_chain_2[:,t] = val_2.tau
        tau_e_1.append(val_1.tau_e)
        tau_e_2.append(val_2.tau_e)
        
        
        np.random.seed()
        aux_r = np.random.randint(1,5000, size = K+1)
        
        pred_1 = 0
        pred_2 = 0
        if dist[t]>4:
            coupling= True
            if verbose:
                print("try at t=",t)
                print("a[k]:",sum([sum(np.power(val_1.a[k]-val_1.a[k],2)) for k in range(len(val_1.a))]))
                print("mu:",(val_1.mu-val_2.mu)**2)
                print("tau_e:",(val_1.tau_e-val_2.tau_e)**2)
                print("tau_k:",sum(np.power(val_1.tau-val_2.tau,2)))
        else:
            coupling=False



        for k in range(data.K):
            s_k_1 = data.N_s[k]*val_1.tau_e / (val_1.tau[k]+ data.N_s[k]*val_1.tau_e)
            s_k_2 = data.N_s[k]*val_2.tau_e / (val_2.tau[k]+ data.N_s[k]*val_2.tau_e)
            sum_a_l_1 =  sum_on_all(val_1.a,data.N_sl,k, data.N_s,data.iss)
            sum_a_l_2 =  sum_on_all(val_2.a,data.N_sl,k, data.N_s,data.iss)
            #print(sum_a_l_1,sum_a_l_2 )
            
            if not coupling:
                np.random.seed(aux_r[k])
                val_1.tau[k] = np.random.gamma(size=1, shape= data.iss[k]/2-0.5, scale= 2/sum(np.power(val_1.a[k],2)))
                np.random.seed(aux_r[k])
                val_2.tau[k] = np.random.gamma(size=1, shape= data.iss[k]/2-0.5, scale= 2/sum(np.power(val_2.a[k],2)))
            else:
                val_1.tau[k], val_2.tau[k] = maximal_coupling_gamma(data.iss[k]/2-0.5,data.iss[k]/2-0.5,2/sum(np.power(val_1.a[k],2)),2/sum(np.power(val_2.a[k],2)))
                if verbose:
                    print(t," tau coupling:",k," ", val_1.tau[k]==val_2.tau[k]," ", 2/sum(np.power(val_1.a[k],2)),2/sum(np.power(val_2.a[k],2)))
    

            if collapsed:
                mu_mean_1= sum( s_k_1 *(data.mean_y_s[k] - sum_a_l_1))/sum(s_k_1)
                mu_mean_2= sum( s_k_2 *(data.mean_y_s[k] - sum_a_l_2))/sum(s_k_2)
                var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                var_mean_2 = 1/(val_2.tau[k]*sum(s_k_2))
                if not coupling:
                   

                    aux = np.random.normal(0,1)
                    val_1.mu = mu_mean_1+aux*np.sqrt(var_mean_1)
                    val_2.mu = mu_mean_2+aux*np.sqrt(var_mean_2)
                else:
                   
                    val_1.mu, val_2.mu = maximal_coupling_normal(mu_mean_1, mu_mean_2, var_mean_1, var_mean_2)

            a_k_mean_1 = val_1.tau_e*data.N_s[k]/(val_1.tau_e*data.N_s[k]+val_1.tau[k])*(np.array(data.mean_y_s[k])-val_1.mu-sum_a_l_1)
            a_k_var_1 = 1/(data.N_s[k]*val_1.tau_e+val_1.tau[k])  

            a_k_mean_2 = val_2.tau_e*data.N_s[k]/(val_2.tau_e*data.N_s[k]+val_2.tau[k])*(np.array(data.mean_y_s[k])-val_2.mu-sum_a_l_2)
            a_k_var_2 = 1/(data.N_s[k]*val_2.tau_e+val_2.tau[k])  
            
            if not coupling:
                aux = np.random.normal(0,1, size= data.iss[k])
                val_1.a[k]= a_k_mean_1+ np.sqrt(a_k_var_1)*aux
                val_2.a[k]= a_k_mean_2+ np.sqrt(a_k_var_2)*aux
                a_means_1[k] = sum(val_1.a[k]*data.N_s[k])/N
                a_means_2[k] = sum(val_2.a[k]*data.N_s[k])/N
                
            else:
                if (val_1.tau[k]==val_2.tau[k]).all():
                    val_1.a[k], val_2.a[k] = reflection_coupling(a_k_mean_1, a_k_mean_2, np.diag(a_k_var_1))
                else:
                    for o in range(len(val_1.a[k])):
                        val_1.a[k][o], val_2.a[k][o] = maximal_coupling_normal(a_k_mean_1[o], a_k_mean_2[o], a_k_var_1[o], a_k_var_2[o])
            


            if PX and not coupling:
                prec_alpha_k_1= val_1.tau_e*sum(data.N_s[k]*np.power(val_1.a[k],2))
                mean_alpha_k_1 = sum(val_1.a[k]*(data.N_s[k]*val_1.tau_e+val_1.tau[k])*a_k_mean_1)/prec_alpha_k_1

                prec_alpha_k_2= val_2.tau_e*sum(data.N_s[k]*np.power(val_2.a[k],2))
                mean_alpha_k_2 = sum(val_2.a[k]*(data.N_s[k]*val_2.tau_e+val_2.tau[k])*a_k_mean_2)/prec_alpha_k_2

                aux = np.random.normal(0,1, size= 1)

                alpha_k_1 = mean_alpha_k_1+ aux/np.sqrt(prec_alpha_k_1)
                alpha_k_2 = mean_alpha_k_2+ aux/np.sqrt(prec_alpha_k_2)
                val_1.a[k] = val_1.a[k]*alpha_k_1
                val_2.a[k] = val_2.a[k]*alpha_k_2

            pred_1 += val_1.a[k][data.ii[:,k]-1]
            pred_2 += val_2.a[k][data.ii[:,k]-1]
            
            
        pred_1 += val_1.mu
        pred_2 += val_2.mu
        SS0_1 = sum(np.power((data.y-pred_1),2))
        SS0_2 = sum(np.power((data.y-pred_2),2))
        
       
        if not coupling:
            np.random.seed(aux_r[K])
            val_1.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_1)
            np.random.seed(aux_r[K])
            val_2.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_2)
        else:
            val_1.tau_e, val_2.tau_e = maximal_coupling_gamma(N/2-0.5,N/2-0.5,2/SS0_1, 2/SS0_2)

            
        SS0_chain_1.append(SS0_1)
        SS0_chain_2.append(SS0_2)
    
        dist.append( distance(val_1, val_2, data))
   
        
        if (val_1.mu == val_2.mu) and np.array([(val_1.a[k] == val_2.a[k]).all() for k in range(data.K)]).all() and val_1.tau_e==val_2.tau_e and np.array(val_1.tau == val_2.tau).all() :
            print("yeeeee, ",t)           
            break
        if verbose:
            print("end of t=",t)
            
        
    t_fin = t
    if t_fin+1 < T: #means coupling reached
        
        mu = np.sum(mu_chain_1[int(t_fin/2):t_fin])
        tau_e = np.sum(tau_e_1[int(t_fin/2):t_fin])
        tau_k = np.sum(tau_chain_1[:,int(t_fin/2):t_fin])
        a_k_means = np.sum(a_means_chain_1[:,int(t_fin/2):t_fin])
    
        
        #print(tau_chain_1[0,t_fin-5:t_fin+5], tau_chain_2[0,t_fin-5:t_fin+5])
        #print(tau_chain_1[1,t_fin-5:t_fin+5], tau_chain_2[1,t_fin-5:t_fin+5])
        #print(tau_chain_1[:,t_fin]-tau_chain_2[:,t_fin])
        #print(mu_chain_1[t_fin]-mu_chain_2[t_fin])
         
        for m in range(int(t_fin/2)+1, t_fin):
            for t in range(m,t_fin):
                mu += mu_chain_1[t]-mu_chain_2[t]

            for t in range(m,t_fin):
                tau_e += tau_e_1[t]-tau_e_2[t]
            

            for t in range(m,t_fin):
                tau_k+= tau_chain_1[:,t]-tau_chain_2[:,t]
            
            for t in range(m,t_fin):
                a_k_means += a_means_chain_1[:,t]-a_means_chain_2[:,t]
             
        
        
        return mu/(t_fin-int(t_fin/2)+1),tau_e/(t_fin-int(t_fin/2)+1), tau_k/(t_fin-int(t_fin/2)+1),a_k_means/(t_fin-int(t_fin/2)+1), dist, t_fin
    else:
        return 0



def MCMC_sampler_final_new(data,T, collapsed, PX,L=1, verbose= False, eps = False):
    
    N = data.N
    K = data.K
    val_1 = iter_value_r(data.I,data.K, data.iss)
    val_2 = iter_value_r(data.I,data.K, data.iss)

    a_means_1 = [sum(val_1.a[k] * data.N_s[k])/N for k in range(data.K)]
    a_means_2 = [sum(val_2.a[k] * data.N_s[k])/N for k in range(data.K)]
    mu_chain_1 = []
    mu_chain_2 = []
    tau_chain_1= np.zeros((K,T))
    tau_chain_2= np.zeros((K,T))
    tau_e_1 = []
    tau_e_2 = []
    a_means_chain_1 = np.zeros(shape=(data.K,T))
    a_means_chain_2 = np.zeros(shape=(data.K,T))
    SS0_chain_1 = []
    SS0_chain_2 = []
    dist = [distance(val_1, val_2, data)]
    t_fin = 0
    coupling = False
    
    
    #add the lag in the coupling, only in one chain
    
    for t in range(L): #need to sample only from one chain
        pred_1 = 0
        for k in range(data.K):
            s_k_1 = data.N_s[k]*val_1.tau_e / (val_1.tau[k]+ data.N_s[k]*val_1.tau_e)
            sum_a_l_1 =  sum_on_all(val_1.a,data.N_sl,k, data.N_s,data.iss)
            
            if collapsed:
                mu_mean_1= sum( s_k_1 *(data.mean_y_s[k] - sum_a_l_1))/sum(s_k_1)
                var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                aux = np.random.normal(0,1)
                val_1.mu = mu_mean_1+aux*np.sqrt(var_mean_1)
                    
               
            a_k_mean_1 = val_1.tau_e*data.N_s[k]/(val_1.tau_e*data.N_s[k]+val_1.tau[k])*(np.array(data.mean_y_s[k])-val_1.mu-sum_a_l_1)
            a_k_var_1 = 1/(data.N_s[k]*val_1.tau_e+val_1.tau[k])  
            aux = np.random.normal(0,1, size= data.iss[k])
            val_1.a[k]= a_k_mean_1+ np.sqrt(a_k_var_1)*aux
            a_means_1[k] = sum(val_1.a[k]*data.N_s[k])/N

            if PX :
                prec_alpha_k_1= val_1.tau_e*sum(data.N_s[k]*np.power(val_1.a[k],2))
                mean_alpha_k_1 = sum(val_1.a[k]*(data.N_s[k]*val_1.tau_e+val_1.tau[k])*a_k_mean_1)/prec_alpha_k_1
                aux = np.random.normal(0,1, size= 1)
                alpha_k_1 = mean_alpha_k_1+ aux/np.sqrt(prec_alpha_k_1)
                val_1.a[k] = val_1.a[k]*alpha_k_1

            pred_1 += val_1.a[k][data.ii[:,k]-1]
            val_1.tau[k] = np.random.gamma(size=1, shape= data.iss[k]/2-0.5, scale= 2/sum(np.power(val_1.a[k],2)))
            
        pred_1 += val_1.mu
        SS0_1 = sum(np.power((data.y-pred_1),2))
        val_1.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_1)
    
    # now start the algorithm for sampling 
    for t in range(T):
        print(t, end='\r')

        mu_chain_1.append(val_1.mu)
        mu_chain_2.append(val_2.mu)
        a_means_chain_1[:,t]=a_means_1
        a_means_chain_2[:,t]=a_means_2
        tau_chain_1[:,t] = val_1.tau
        tau_chain_2[:,t] = val_2.tau
        tau_e_1.append(val_1.tau_e)
        tau_e_2.append(val_2.tau_e)
        
        
        np.random.seed()
        aux_r = np.random.randint(1,5000, size = K+1)
        
        pred_1 = 0
        pred_2 = 0
        if dist[t]>4:
            coupling= True
            if verbose:
                print("try at t=",t)
                print("a[k]:",sum([sum(np.power(val_1.a[k]-val_1.a[k],2)) for k in range(len(val_1.a))]))
                print("mu:",(val_1.mu-val_2.mu)**2)
                print("tau_e:",(val_1.tau_e-val_2.tau_e)**2)
                print("tau_k:",sum(np.power(val_1.tau-val_2.tau,2)))
        else:
            coupling=False



        for k in range(data.K):
            s_k_1 = data.N_s[k]*val_1.tau_e / (val_1.tau[k]+ data.N_s[k]*val_1.tau_e)
            s_k_2 = data.N_s[k]*val_2.tau_e / (val_2.tau[k]+ data.N_s[k]*val_2.tau_e)
            sum_a_l_1 =  sum_on_all(val_1.a,data.N_sl,k, data.N_s,data.iss)
            sum_a_l_2 =  sum_on_all(val_2.a,data.N_sl,k, data.N_s,data.iss)
            #print(sum_a_l_1,sum_a_l_2 )
            
            if not coupling:
                # try now to rascale one sample from gamma
                aux = np.random.gamma(size = 1, shape= data.iss[k]/2-0.5, scale=1)
                val_1.tau[k] = 2/sum(np.power(val_1.a[k],2))*aux
                val_2.tau[k] = 2/sum(np.power(val_2.a[k],2))*aux
            else:
                val_1.tau[k], val_2.tau[k] = maximal_coupling_gamma(data.iss[k]/2-0.5,data.iss[k]/2-0.5,2/sum(np.power(val_1.a[k],2)),2/sum(np.power(val_2.a[k],2)))
                if verbose:
                    print(t," tau coupling:",k," ", val_1.tau[k]==val_2.tau[k]," ", 2/sum(np.power(val_1.a[k],2)),2/sum(np.power(val_2.a[k],2)))
    

            if collapsed:
                mu_mean_1= sum( s_k_1 *(data.mean_y_s[k] - sum_a_l_1))/sum(s_k_1)
                mu_mean_2= sum( s_k_2 *(data.mean_y_s[k] - sum_a_l_2))/sum(s_k_2)
                var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                var_mean_2 = 1/(val_2.tau[k]*sum(s_k_2))
                if not coupling:
                   

                    aux = np.random.normal(0,1)
                    val_1.mu = mu_mean_1+aux*np.sqrt(var_mean_1)
                    val_2.mu = mu_mean_2+aux*np.sqrt(var_mean_2)
                else:
                   
                    val_1.mu, val_2.mu = maximal_coupling_normal(mu_mean_1, mu_mean_2, var_mean_1, var_mean_2)

            a_k_mean_1 = val_1.tau_e*data.N_s[k]/(val_1.tau_e*data.N_s[k]+val_1.tau[k])*(np.array(data.mean_y_s[k])-val_1.mu-sum_a_l_1)
            a_k_var_1 = 1/(data.N_s[k]*val_1.tau_e+val_1.tau[k])  

            a_k_mean_2 = val_2.tau_e*data.N_s[k]/(val_2.tau_e*data.N_s[k]+val_2.tau[k])*(np.array(data.mean_y_s[k])-val_2.mu-sum_a_l_2)
            a_k_var_2 = 1/(data.N_s[k]*val_2.tau_e+val_2.tau[k])  
            
            if not coupling:
                aux = np.random.normal(0,1, size= data.iss[k])
                val_1.a[k]= a_k_mean_1+ np.sqrt(a_k_var_1)*aux
                val_2.a[k]= a_k_mean_2+ np.sqrt(a_k_var_2)*aux
                a_means_1[k] = sum(val_1.a[k]*data.N_s[k])/N
                a_means_2[k] = sum(val_2.a[k]*data.N_s[k])/N
                
            else:
                if (val_1.tau[k]==val_2.tau[k]).all():
                    val_1.a[k], val_2.a[k] = reflection_coupling(a_k_mean_1, a_k_mean_2, np.diag(a_k_var_1))
                else:
                    for o in range(len(val_1.a[k])):
                        val_1.a[k][o], val_2.a[k][o] = maximal_coupling_normal(a_k_mean_1[o], a_k_mean_2[o], a_k_var_1[o], a_k_var_2[o])
            


            if PX and not coupling:
                prec_alpha_k_1= val_1.tau_e*sum(data.N_s[k]*np.power(val_1.a[k],2))
                mean_alpha_k_1 = sum(val_1.a[k]*(data.N_s[k]*val_1.tau_e+val_1.tau[k])*a_k_mean_1)/prec_alpha_k_1

                prec_alpha_k_2= val_2.tau_e*sum(data.N_s[k]*np.power(val_2.a[k],2))
                mean_alpha_k_2 = sum(val_2.a[k]*(data.N_s[k]*val_2.tau_e+val_2.tau[k])*a_k_mean_2)/prec_alpha_k_2

                aux = np.random.normal(0,1, size= 1)

                alpha_k_1 = mean_alpha_k_1+ aux/np.sqrt(prec_alpha_k_1)
                alpha_k_2 = mean_alpha_k_2+ aux/np.sqrt(prec_alpha_k_2)
                val_1.a[k] = val_1.a[k]*alpha_k_1
                val_2.a[k] = val_2.a[k]*alpha_k_2

            pred_1 += val_1.a[k][data.ii[:,k]-1]
            pred_2 += val_2.a[k][data.ii[:,k]-1]
            
            
        pred_1 += val_1.mu
        pred_2 += val_2.mu
        SS0_1 = sum(np.power((data.y-pred_1),2))
        SS0_2 = sum(np.power((data.y-pred_2),2))
        
        
              
                
       
        if not coupling:
            aux = np.random.gamma(size = 1, shape= N/2-0.5, scale=1)
            val_1.tau_e = 2/SS0_1*aux
            val_2.tau_e = 2/SS0_2*aux
        else:
            val_1.tau_e, val_2.tau_e = maximal_coupling_gamma(N/2-0.5,N/2-0.5,2/SS0_1, 2/SS0_2)

            
        SS0_chain_1.append(SS0_1)
        SS0_chain_2.append(SS0_2)
    
        dist.append( distance(val_1, val_2, data))
   
        
        if (val_1.mu == val_2.mu) and np.array([(val_1.a[k] == val_2.a[k]).all() for k in range(data.K)]).all() and val_1.tau_e==val_2.tau_e and np.array(val_1.tau == val_2.tau).all() :
            print("yeeeee, ",t)           
            break
        if verbose:
            print("end of t=",t)
            
        
    t_fin = t
    if t_fin+1 < T: #means coupling reached
        
        mu = np.sum(mu_chain_1[int(t_fin/2):t_fin])
        tau_e = np.sum(tau_e_1[int(t_fin/2):t_fin])
        tau_k = np.sum(tau_chain_1[:,int(t_fin/2):t_fin])
        a_k_means = np.sum(a_means_chain_1[:,int(t_fin/2):t_fin])
    
        
        #print(tau_chain_1[0,t_fin-5:t_fin+5], tau_chain_2[0,t_fin-5:t_fin+5])
        #print(tau_chain_1[1,t_fin-5:t_fin+5], tau_chain_2[1,t_fin-5:t_fin+5])
        #print(tau_chain_1[:,t_fin]-tau_chain_2[:,t_fin])
        #print(mu_chain_1[t_fin]-mu_chain_2[t_fin])
         
        for m in range(int(t_fin/2)+1, t_fin):
            for t in range(m,t_fin):
                mu += mu_chain_1[t]-mu_chain_2[t]

            for t in range(m,t_fin):
                tau_e += tau_e_1[t]-tau_e_2[t]
            

            for t in range(m,t_fin):
                tau_k+= tau_chain_1[:,t]-tau_chain_2[:,t]
            
            for t in range(m,t_fin):
                a_k_means += a_means_chain_1[:,t]-a_means_chain_2[:,t]
             
        if eps:   
            return mu/(t_fin-int(t_fin/2)+1),tau_e/(t_fin-int(t_fin/2)+1), tau_k/(t_fin-int(t_fin/2)+1),a_k_means/(t_fin-int(t_fin/2)+1), dist, t_fin, t_eps
        else:
            return mu/(t_fin-int(t_fin/2)+1),tau_e/(t_fin-int(t_fin/2)+1), tau_k/(t_fin-int(t_fin/2)+1),a_k_means/(t_fin-int(t_fin/2)+1), dist, t_fin, t_eps
    else:
        return 0







